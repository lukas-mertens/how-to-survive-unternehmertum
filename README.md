# How to survive Unternehmertum
> **Achtung** Diese Liste dient nicht als Rechtsberatung und wird von juristischen Laien geführt! Die Ersteller dieser Liste haben auch keine Ahnung,  so take everything with two grains of salt, because one grain is probably not enough!

Während unserer Gründungsvorhaben, haben wir die Erfahrung gemacht, dass es oft schwer ist, den Überblick über alle gesetzlichen Vorgaben und Regularien zu behalten. Ein häufiges Problem ist nicht etwa die Gesetze tatsächlich einzuhalten, sondern überhaupt erstmal zu wissen, dass sie existieren. In diesem Repository soll mit der Zeit eine Übersicht über Regularien entstehen, die für Gründungsvorhaben wichtig sind. Der Anspruch liegt dabei nicht bei der Vollständigkeit und ausführlichen Erklärung. Stattdessen sollen Stichworte mit Links zu externen, ausführlichen und vor allem wahrscheinlich deutlich korrekteren Erklärungen gesammelt werden.

## Lesen

Zum lesen, gehe [hier](https://lukas-mertens.gitlab.io/how-to-survive-unternehmertum) hin...

## Mithelfen
Wir freuen uns über jegliche Mithilfe! Content kannst du [hier](content) bearbeiten...
