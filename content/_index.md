---
title: Introduction
type: docs
---

# How to survive Unternehmertum
{{< hint danger >}}
**Achtung** Diese Liste dient nicht als Rechtsberatung und wird von juristischen Laien geführt! Die Ersteller dieser Liste haben auch keine Ahnung,  so take everything with two grains of salt, because one grain is probably not enough!
{{< /hint >}}

## Inhalt

### Unternehmensformen
 - [Unternehmergesellschaft (haftungsbeschränkt) (UG)](docs/Unternehmensformen/Unternehmergesellschaft)
 - [Gesellschaft mit beschränkter Haftung (GmbH)](docs/Unternehmensformen/GmbH)
  - [Aktiengesellschaft (AG)](docs/Unternehmensformen/Aktiengesellschaft)

### Allgemein wichtig für alle
 - [DSGVO](docs/DSGVO/)
 - [GoBD](docs/GoBD/)
 - [Buchhaltung](docs/Buchhaltung)
