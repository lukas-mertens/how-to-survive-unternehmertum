---
weight: 1
bookFlatSection: true
title: "HomeOffice"
---

# Arbeitsschutz - Home Office

## Gefährdungsbeurteilung

- Eine der folgenden Personen muss Zutrittsberechtigung erhalten:
  - Arbeitgeber
  - Fachkraft Arbeitssicherheit oder
  - Betriebsarzt

## Ausnahmen des Arbeitsschutzes

- Bildschirmgeräten für den ortsveränderlichen Gebrauch, sofern sie nicht regelmäßig an einem Arbeitsplatz eingesetzt werden ([Arbeitsrechte](https://www.arbeitsrechte.de/bildschirmarbeitsverordnung/))

## Effektive Pflichten

1. Bildschirmgerät und Tastatur: Hier geht es unter anderem darum, dass die Tastatur über eine reflexionsarme Oberfläche verfügt, die Hände auf der Arbeitsfläche davor abgelegt werden können oder dass sich die Beschriftung der Tasten vom Untergrund abhebt. Des Weiteren besagt die Arbeitsplatzverordnung in puncto Bildschirm, dass das Bild nicht flimmern darf und die dargestellten Zeichen deutlich erkennbar sein müssen.

2. Sonstige Arbeitsmittel: Bei diesem Punkt sieht die Bildschirmarbeitsverordnung einen ergonomischen und standsicheren Stuhl, eine ausreichend große und reflexionsarme Arbeitsfläche und auf Wunsch eine Fußstütze vor.

3. Arbeitsumgebung: Ausreichend Raum, um eine andere Haltung oder anderweitige Bewegungen ausführen zu können, entsprechende Beleuchtungen, die nicht blenden, eine gewisse Luftfeuchtigkeit und das Vermeiden von Lärm, der durch die benötigten Arbeitsmittel entsteht, fallen laut Bildschirmarbeitsverordnung in diese Kategorie.

4. Zusammenwirken Mensch – Arbeitsmittel: Hier dreht es sich unter anderem um eine der Tätigkeit entsprechende und benutzerfreundliche Software, die je nach vorhandenen Kenntnissen und Erfahrungen des Mitarbeiters angepasst werden kann. Unwissentlich dürfen diese außerdem nicht durch eine bestimmte Vorrichtung der Software kontrolliert werden

## Interessantes

- *Eine weitere Pflicht des Arbeitgebers besteht laut Bildschirmarbeitsverordnung darin, den Beschäftigten in regelmäßigen Abständen Untersuchungen der Augen zu offerieren. Je nachdem, wie alt der betroffene Arbeitnehmer ist, sollten die Zeitabstände angepasst werden.<br>
Unter 40 Jahren sind es alle fünf, über 40 alle drei Jahre. Bei Beschwerden, die das Sehvermögen des Mitarbeiters betreffen und auf die Arbeit vor dem Monitor zurückzuführen sind, müssen ebenfalls Untersuchungen angeboten werden.* ([Arbeitsrechte](https://www.arbeitsrechte.de/bildschirmarbeitsverordnung/))

- *Sollten gewöhnliche bzw. schon vorhandene Sehhilfen nicht ausreichen, um die Arbeit verrichten zu können, steht Arbeitnehmern außerdem laut Bildschirmarbeitsplatzverordnung eine kostenlose Brille zu.* ([Arbeitsrechte](https://www.arbeitsrechte.de/bildschirmarbeitsverordnung/))

- *Der Arbeitgeber hat die Tätigkeit der Beschäftigten so zu organisieren, daß die tägliche Arbeit an Bildschirmgeräten regelmäßig durch andere Tätigkeiten oder durch Pausen unterbrochen wird, die jeweils die Belastung durch die Arbeit am Bildschirmgerät verringern.“* (Jedoch sind Pausen nicht näher spezifiziert! Aber s.u.) ([Arbeitsrechte](https://www.arbeitsrechte.de/bildschirmarbeitsverordnung/))

- *Die in der Bildschirmarbeitsverordnung vorgeschriebenen Pausen sollten möglichst fünf bis zehn Minuten andauern und jede Stunde einmal eingelegt werden. Eine Mittagspause ist ohnehin Pflicht. <br>
Mehrere kurze Pausen bieten sich hier eher an als längere Pausen, damit die oft starre Sitzhaltung und die Fixierung auf den Bildschirm in regelmäßigen Abständen unterbrochen werden.* ([Arbeitsrechte](https://www.arbeitsrechte.de/bildschirmarbeitsverordnung/))

- *Der Arbeitsschutz am Bildschirmarbeitsplatz kann grundsätzlich nicht gewährleistet werden, wenn Arbeitnehmer länger als zwei Stunden Tätigkeiten vor dem Monitor ausüben.* ([Arbeitsrechte](https://www.arbeitsrechte.de/bildschirmarbeitsverordnung/))

- *„Ein Notebook ist aus ergonomischer Sicht eher ungeeignet zum täglichen Arbeiten“, so der Experte. Ein externer Bildschirm und eine externe Tastatur müssen in diesem Fall vom Arbeitgeber bereitgestellt, Arbeitsmittel wie Drucker oder Aktenvernichter in geeigneten Intervallen geprüft und gewartet werden.* ([Arbeitsschutz-Portal](https://www.arbeitsschutz-portal.de/beitrag/asp_news/5483/regeln-fuer-die-arbeitssicherheit-im-home-office.html))
- *Mit dieser deutlich enger gefassten Definition wird gleichzeitig klargestellt, dass beruflich bedingte „mobile Arbeit”, bspw. das gelegentliche Arbeiten mit dem Laptop von zu Hause, das ortsungebundene Arbeiten im Café oder unterwegs im Zug, nicht vom Anwendungsbereich der Arbeitsstättenverordnung erfasst wird.* ([Roedl](https://www.roedl.de/themen/arbeitsstaettenverordnung-home-office-versicherung))
