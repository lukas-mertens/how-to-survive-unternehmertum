---
weight: 1
bookFlatSection: true
title: "Lohnbuchhaltung"
---

# Lohnbuchhaltung

# Sage One Lohnbuchhaltung

## Q&A aus Supportanfragen

### Abrechnung bei flexibler Stundenzahl

**Frage**

> Hallo,
> ich hatte mich vorletzten Monat telefonisch bei Ihnen gemeldet, um mich zu erkundigen, wie ich Werkstudenten zum 20. des Monats abrechnen kann, die ihre Stundenzahl für die Tage 21-31 des jeweiligen Monats logischerweise nicht wissen. Mir wurde gesagt, dass ich dies einfach machen könnte, indem ich die jeweiligen Stundenzahlen bei den Mitarbeitern nach Tagen aufgeschlüsselt eintrage und dann die Stunden für Tag 21-31 auf 0 lasse. Bei der nächsten Lohnabrechnung passe ich diese dann entsprechend an. Es wird eine Korrekturmeldung für die restlichen Tage des vorherigen Monats erstellt. Mir wurde gesagt, diese sei dann jedoch kostenlos. Nun wurden mir diese Korrekturen aber trotzdem in Rechnung gestellt. Wie mache ich es denn nun richtig?

**Antwort**

> grundsätzlich empfehlen wir die Abrechnung erst zum Monatsende, also dem Zeitpunkt, zu dem Sie alle Abrechnungsdaten kennen. Für die rechtzeitige Übermittlung der Kassenmeldungen gibt es im System die Möglichkeit der "Beitragsschätzung" anhand der zu diesem Zeitpunkt hinterlegten Daten. Hierbei wird nur die Kassenmeldung abgesetzt und kein Monatswechsel gemacht. Differenzen zur späteren Echtabrechnung mit vollständigen Werten, werden im Zuge der Meldung des Folgemonats verrechnet. Korrekturen sind hierbei nicht nötig, da Sie ja direkt (jedoch erst später) die Echtabrechnung mit korrekten Werten vornehmen. Wenn Sie die Echtabrechnung aus bestimmten Gründen nicht später durchführen können, ist die Korrektur der Stunden im Zuge des Folgemonats alternativlos. Diese Abrechnung ist nicht falsch, jedoch stets mit einer kostenpflichtigen Korrektur verbunden.


### Urlaubsberechnung Werkstudenten

**Frage**

> Hallo, ich beschäftige Werkstudenten mit einer monatlich variablen Arbeitszeit. Wie kann ich Urlaub in Stunden erfassen? In Tagen/Halbtagen macht es wenig Sinn, weil die Stundenzahl logischerweise variiert. Wie kann ich in Stunden abrechnen?

**Antwort**

> Die Urlaubsdauer bei einer flexiblen Arbeitszeit berechnen Sie wie folgt: Gesetzliche oder tarifliche Urlaubsdauer in Vollzeit, geteilt durch die Jahreswerktage, multipliziert mit den Tagen, an denen Sie zur Arbeit verpflichtet sind. Folgendes Beispiel verdeutlicht die Berechnung: Sie haben nach dem Gesetz einen Urlaubsanspruch bei einer 6 Tage-Woche von 24 Werktagen. Dieses geteilt durch 312 Jahreswerktage (52 Wochen x 6 Werktage), multipliziert mit Ihren Arbeitstagen, ergibt den Urlaubsanspruch. Ihre Jahresarbeitstage ermitteln Sie danach, wie viel Tage Sie im Jahr gearbeitet haben. Sind dieses beispielsweise 100 Jahresarbeitstage, an denen Sie gearbeitet haben, ergibt sich folgende Rechnung: 24 Tage Urlaubsanspruch : 312 Jahreswerktage = eine Urlaubsdauer von 7,69 Arbeitstagen. Wichtig: Es ist völlig unerheblich, wie viel Stunden an den einzelnen Arbeitstagen gearbeitet wurde. Eine Prüfung auf Korrektheit Ihrer Berechnung lassen Sie bitte von Ihrem Steuerberater vornehmen.
