---
weight: 1
bookFlatSection: true
title: "Minijobber"
---

# Minijobber

## Arbeitszeit
 - Ein Minijobber darf vereinzelt auch mal die 450-Euro-Grenze überschreiten, falls es nicht regelmäßig vorkommt (d.h. weniger als 3 Mal in einem 12-Monats-Zeitraum)
 - In Ausnahmefällen (z.B. Krankheitsvertretung) ist es auch möglich, dass die 5400 Euro-Grenze gesprengt wird, ohne dass der Minijobstatus angegriffen wird

[Quelle](https://www.minijob-zentrale.de/DE/01_minijobs/03_haushalt/01_grundlagen_minijobs_im_privathaushalt/02_450_euro_minijobs_imph/01_entgeltgrenze/node.html#:~:text=Ihr%20Minijobber%20darf%20bis%20zu,bis%20zu%20450%20Euro%20verdienen.&text=%C3%9Cberschreitet%20Ihr%20Arbeitnehmer%20diese%20Verdienstgrenze,Minijob%2C%20sondern%20ein%20sozialversicherungspflichtiges%20Besch%C3%A4ftigungsverh%C3%A4ltnis.)
