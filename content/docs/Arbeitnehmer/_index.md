---
weight: 1
bookFlatSection: true
title: "Arbeitnehmer einstellen"
---

# Arbeitnehmer einstellen
## DGUV Vorschrift 3
Alle elektrischen Arbeitsmittel müssen regelmäßig abgenommen werden...

## Aushangpflichtige Gesetze
Es gibt gewisse Gesetze, die aushangpflichtig sind. Dafür gibt es beispielsweise diverse fertige Buchbände, die alle nötigen Gesetze enthalten. Alternativ findet sich im Folgenden eine Auflistung:

 - [Übersicht über alle aushangpflichtigen Gesetze](https://www.kvsa.de/fileadmin/user_upload/PDF/Praxis/Qualitaetsmanagement/PRO_6_2014_Aushangpflichtige_Gesetze.pdf)

## Werkstudenten
 - Als Werkstudenten/Werkstudentinnen können nur an einer deutschen Hochschule eingeschriebene Vollzeitstudierende eingestellt werden
 - **Achtung** verdient ein/e Werkstudent/in regelmäßig maximal 450€ pro Monat, so muss diese/r als Minijobber abgerechnet werden!
 - Aufgrund der höheren Abgaben, die für Minijobber zu entrichten sind, lohnt sich ein Gehalt zwischen 380-450 Euro für den Arbeitgeber bei Studierenden betriebswirtschaftlich nicht. Ein höheres Gehalt mit einer Abrechnung als Werkstudent ist hier für den Arbeitgeber günstiger.
 - **Achtung** Der Werkstudent sollte prüfen, ob er sich selbst krankenversichern muss (z.B. bei Familienversicherung häufig 450€-Grenze)
  - Werkstudenten dürfen maximal 20 Stunden pro Woche arbeiten, sonst gelten sie automatisch als reguläre Arbeitnehmer!
  - Der Urlaubsanspruch kann [anteilig](https://www.bachelorprint.de/karriere/jobs-stellenangebote/werkstudent/urlaubsanspruch-werkstudent/) berechnet werden.
   - Auch wenn das Studium weiterhin als Hauptbeschäftigung gilt, kann ein Werkstudent nach Ablauf der Probezeit nicht ohne relevanten Grund gekündigt werden. Beendet er das Studium innerhalb des Arbeitsverhältnisses, stellt das keinen Kündigungsgrund dar, es sei denn, dies steht explizit im Vertrag. ([Quelle](https://www.stellenwerk.de/magazin/personalsuche/das-muessen-sie-beachten-wenn-sie-einen-werkstudenten-einstellen))

### Checkliste benötigte Dokumente/Daten
 - ELStAM: elektronische Lohnsteuerabzugsmerkmale
 - Geburtsdatum
 - vollständiger Name
 - IBAN für Gehalt
 - Immatrikulationsbescheinigung
 - Adresse des gemeldeten Wohnsitzes
 - Welche Krankenkasse?
 - Sozialversicherungsausweis (dieser enthält die Rentenversicherungsnummer)
 - Urlaubsbescheinigung des vorherigen Arbeitgebers
     - falls vorhanden, relevant für Berechnung des Urlaubsanspruches, damit Urlaub nicht doppelt gewährt wird/der gesetzliche Mindesturlaub eingehalten wird

## Snacks für Mitarbeiter
Kleinere Snacks und Aufmerksamkeiten, die nicht als Mahlzeit gelten, sind steuerfrei. z.B.:
 - Kekse
 - Mineralwasser
 - Kaffee
usw. Mahlzeiten sind hingegen steuerpflichtig, wenn sie nicht zu besonderen betrieblichen Anlässen (z.B. Inventur) gewährt werden ([Quelle](https://www.haufe.de/personal/entgelt/annehmlichkeiten-kaffee-kekse-und-mineralwasser_78_227514.html)).

