---
weight: 1
bookFlatSection: true
title: "Buchhaltung"
---

# Buchhaltung

## Unvollständige Liste an Buchhaltungstools

 - [lexoffice](https://www.lexoffice.de/)
 - [sevDesk](https://sevdesk.de/ref/?sref=32574773057)
 - [Lexware Buchhaltung](https://shop.lexware.de/buchhaltungssoftware)
 - [scopevisio](https://www.scopevisio.com/)

### Lohnbuchhaltung
 - [Sage Lohnabrechnung](https://www.sage.com/de-de/sage-business-cloud/lohnabrechnung/)
 - 

## Nachschlagwerk
 - [skr03](https://www.collmex.de/skr03.pdf)
 - [skr04](https://www.collmex.de/skr04.pdf)
 - [Einführung Buchhaltung mit vielen Standardbuchungen](https://www.collmex.de/einfuehrung_buchhaltung.html)

## Geringwertige Wirtschaftsgüter

### Trivialsoftware
[Artikel](https://www.unternehmerlexikon.de/trivialsoftware/)

Unter Trivialsoftware versteht man selbständig nutzbare, bewegliche Software, deren Anschaffungswert das Unternehmen höchstens 800 Euro ohne Umsatzsteuer kostet (bis zum 31.12.2017 lag die Grenze bei 410 Euro netto). Im Steuerrecht zählt die Trivialsoftware zu den geringwertigen Wirtschaftsgütern. Trivialsoftware fällt unter den Betriebsaufwand und kann im Anschaffungsjahr vollständig abgeschrieben werden.
