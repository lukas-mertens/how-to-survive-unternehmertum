---
weight: 1
bookFlatSection: true
title: "Geschäftsführer"
---

# Geschäftsführer

{{< hint info >}}
Unter Geschäftsführung (oder Geschäftsleitung) versteht man im Gesellschaftsrecht eine oder mehrere natürliche Personen, die bei Unternehmen oder sonstigen Personenvereinigungen mit der Führung der Geschäfte betraut sind und die Gesellschaft als Organ gerichtlich und außergerichtlich organschaftlich vertreten.
{{</ hint>}}

## Sozialversicherungsstatus
**Achtung!** Ein Gesellschafter-Geschäftsführer ist nicht zwangsläufig sozialversicherungsfrei. Wenn der Geschäftsführer in einer abhängigen Position ist (z.B. weniger als 50% der Anteile besitzt), ist er meist sozialversicherungspflichtig! Der Sozialversicherungsstatus kann kostenlos durch die deutsche Clearingstelle geprüft und bescheinigt werden. Bei GmbH-Geschäftsführern ist diese Prüfung verpflichtend.

 - [clearingstelle.de](https://clearingstelle.de)

## Vergütung
 - Der Gesellschafter-Geschäftsführer darf keine Nacht- oder Feiertagszuschläge bekommen (vGA) ([Quelle](https://steuerbuero-bachmann.de/fuer-gesellschafter-geschaeftsfuehrer-bloss-keine-sonntags-feiertags-und-nachtzuschlaege/))

## Geistige Einschränkungen
Ist der GmbH-Geschäftsführer geisteskrank, sind alle Geschäfte, die er abschließt nichtig. Das gilt auch rückwirkend für Geschäfte, wenn die Geisteskrank erst später festgestellt wird. Der Vertragspartner hat keinen Anspruch auf Entschädigung, außer wenn die Gesellschafter etwas hätten ahnen können. [Buch: Der GmbH-Geschäftsführer, Rocco Julia, Seite 5]