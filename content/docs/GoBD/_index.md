---
weight: 1
bookFlatSection: true
title: "GoBD"
---

# GoBD
 - [Guter Artikel über die Grundlagen](https://medium.com/@marc049/gobd-konform-als-selbst%C3%A4ndiger-die-ultimative-schritt-f%C3%BCr-schritt-anleitung-80f5759f36b8)

## Festschreiben
Nach den neuen GoBD der Finanzverwaltung muss ab 01.01.2015 jedes Unternehmen seine Buchführung monatlich, spätestens bis zum 30. des Folgemonats, festschreiben (unten mehr dazu).
[Quelle](https://www.stb-houben.de/2015/03/die-neuen-gobd-buchfuehrungs-regeln-fuer-fast-alle/)

## E-Mail Archivierung
Geschäftliche E-Mails müssen ebenfalls GoBD-Konform archiviert werden.
 - [Artikel über E-Mail Archivierung](https://www.billomat.com/magazin/wie-archiviere-ich-e-mails-gobd-konform/#)


